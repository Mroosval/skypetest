<?php
require_once 'fileImport.php';

class skypeTestTest extends \PHPUnit_Framework_TestCase {
  public $file_import;


  public function setUp() {
    $this->file_import = new fileImport('container', 'container', 'localhost', 'file_import', 'tests');
  }

  public function testCorrectReadInputFile() {
    $file_import = $this->file_import;

    $file = $file_import->readInputFile('client_data/accountinfo.csv');

    $this->assertTrue(count($file) == 6, 'Rows count in file does not match');
  }

  /**
   * @expectedException Exception
   */
  public function testFalseReadInputFile() {
    $file_import = $this->file_import;
    $file_import->readInputFile('client_data/accountinfo2.csv');

    $this->expectExceptionMessage('File does not exist');
  }

  /**
   * @expectedException Exception
   */
  public function testfileTypeReadInputFile() {
    $file_import = $this->file_import;
    $file_import->readInputFile('client_data/accountinfo2.jpg');

    $this->expectExceptionMessage('File type is not correct!');
  }

  public function testinsertLine() {
    $file_import = $this->file_import;

    $data = [
      25215262333,
      '2016/05/11 17:17:36 +0100',
      5999.2
    ];

    $file_import->insertLine($data);

    $lines = $file_import->readLineByTid(25215262333);
    $first_line = $lines[0];

    $this->assertTrue($first_line['transaction_id'] == 25215262333);
    $this->assertTrue($first_line['transaction_date'] == '2016-05-11 17:17:36');
    $this->assertTrue($first_line['amount'] == 5999.2);
  }

}