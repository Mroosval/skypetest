<?php

class fileImport {

  // Database connection
  private $conn;

  // Table to import
  private $table;

  /**
   * fileImport constructor.
   * @param $pass
   * @param $user
   * @param $host
   * @param $db
   */
  public function __construct($pass, $user, $host, $db, $table) {
    $this->conn = new mysqli($host, $user, $pass, $db);
    $this->table = $table;
  }

  /**
   * @param $file_location
   * Imports lines from csv file
   */
  public function importFile($file_location) {
    $file_content = $this->readInputFile($file_location);

    unset($file_content[0]);
    foreach ($file_content as $line) {
      $this->insertLine(str_getcsv($line));
    }
  }

  public function insertLine($data) {
    $conn = $this->conn;

    $sql = 'INSERT INTO ' .  $this->table . ' (transaction_id, transaction_date, amount) VALUES (?, ?, ?)';

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('isd', $data[0], $data[1], $data[2]);

    $stmt->execute();

    $result = $stmt->get_result();
    return $result;
  }

  public function readInputFile($file_location) {
    if (!file_exists($file_location)) {
      throw new Exception('File does not exist');
    }

    $types = array('csv');
    $tmp = explode('.', $file_location);

    if(!in_array(end($tmp), $types)) {
      throw new Exception('File type is not correct!');
    }

    return file($file_location);
  }

  /**
   * @param $tid
   * Read line by transaction id
   */
  public function readLineByTid(  $tid) {
    $results = [];
    $conn = $this->conn;

    $sql = 'SELECT * FROM ' .  $this->table . ' WHERE transaction_id = ?';

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $tid);

    $stmt->execute();

    $meta = $stmt->result_metadata();

    while ($field = $meta->fetch_field()) {
      $parameters[] = &$row[$field->name];
    }

    call_user_func_array(array($stmt, 'bind_result'), $parameters);

    while ($stmt->fetch()) {
      foreach($row as $key => $val) {
        $x[$key] = $val;
      }
      $results[] = $x;
    }

    return $results;
  }
}