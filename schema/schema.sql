SET NAMES utf8;
SET time_zone = '+00:00';

CREATE DATABASE `file_import`;
USE `file_import`;

DROP TABLE IF EXISTS `accounting_data`;
CREATE TABLE `accounting_data` (
  `fid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `amount` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

